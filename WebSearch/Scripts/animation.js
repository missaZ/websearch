﻿$(document).ready(function () {
    $("#ar").click(function () {
        $("#content_kr").hide(2000);
        $("#content_ar").show(2000);
        $("#inst_ar").show(2000);
        $("#inst_kr").hide(2000);
    });

    $("#kr").click(function () {
        $("#content_kr").show(2000);
        $("#content_ar").hide(2000);
        $("#inst_ar").hide(2000);
        $("#inst_kr").show(2000);
    });

    $("input[id$='txtVoterIdAr']").focus(function () {
        $("#firstAr").css({ 'background': 'yellow' });
        $("#secondAr").css({ 'background': '#FFFFFF' });
        $("#ThiddAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtVoterIdAr']").blur(function () {
        $("#firstAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtNameAr']").focus(function () {
        $("#secondAr").css({ 'background': '#FFFFFF' });
        $("#firstAr").css({ 'background': '#FFFFFF' });
        $("#thirdAr").css({ 'background': 'yellow' });
     });

    $("input[id$='txtNameAr']").blur(function () {
        $("#thirdAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtFatherNameAr']").focus(function () {
        $("#thirdAr").css({ 'background': 'yellow' });
        $("#firstAr").css({ 'background': '#FFFFFF' });
        $("#secondAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtFatherNameAr']").blur(function () {
        $("#thirdAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtGrandFatherNameAr']").focus(function () {
        $("#thirdAr").css({ 'background': 'yellow' });
        $("#firstAr").css({ 'background': '#FFFFFF' });
        $("#secondAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtGrandFatherNameAr']").blur(function () {
        $("#thirdAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='ddlGovAr']").focus(function () {
        $("#thirdAr").css({ 'background': 'yellow' });
        $("#firstAr").css({ 'background': '#FFFFFF' });
        $("#secondAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='ddlGovAr']").blur(function () {
        $("#thirdAr").css({ 'background': '#FFFFFF' });
    });

    $("input[id$='txtVRCAr']").focus(function () {
        $("#secondAr").css({ 'background': 'yellow' });
        $("#firstAr").css({ 'background': '#FFFFFF' });
        $("#thirdAr").css({ 'background': '#FFFFFF' });   
    });

    $("input[id$='txtVRCAr']").blur(function () {
        $("#secondAr").css({ 'background': '#FFFFFF' }); 
    });

    $("input[id$='txtFamilyNoAr']").focus(function () {
            $("#secondAr").css({ 'background': 'yellow' });
            $("#firstAr").css({ 'background': '#FFFFFF' });
            $("#thirdAr").css({ 'background': '#FFFFFF' });
     });

    $("input[id$='txtFamilyNoAr']").blur(function () {
            $("#secondAr").css({ 'background': 'yellow' });
            $("#firstAr").css({ 'background': '#FFFFFF' });
            $("#thirdAr").css({ 'background': '#FFFFFF' });
    });
 });
