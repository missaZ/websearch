﻿
function validateInput() {
    if (isNaN($("input[id$='txtVoterIdAr']").val())) {
        alert('رقم الناخب يجب ان يكون ارقام فقط!');
        return false;
    }
           
    if (isNaN($("input[id$='txtFamilyNoAr']").val())) {
        alert('رقم البطاقة التموينية يجب ان يكون ارقام فقط!');
        return false;
    }
   
    if (isNaN($("input[id$='txtVRCAr']").val())) {
        alert('رقم مركز التموين يجب ان يكون ارقام فقط!');
        return false;
    }

    if (($("input[id$='txtVoterIdAr']").val() == "") && ($("input[id$='txtFamilyNoAr']").val() == "") && ($("input[id$='txtVRCAr']").val() == "") && ($("input[id$='txtNameAr']").val() == "") && ($("input[id$='txtFatherNameAr']").val() == "") && ($("input[id$='txtGrandFatherNameAr']").val() == "") ) {
        alert('لايجوز ترك الحقول فارغه!');
        return false;
    }

    return true;
}

function ValidateInputKr() {

    if (isNaN($("input[id$='txtVoterIdKr']").val())) {
        alert('رقم الناخب يجب ان يكون ارقام فقط!');
        return false;
    }

    if (isNaN($("input[id$='txtFamilyNoKr']").val())) {
        alert('رقم البطاقة التموينية يجب ان يكون ارقام فقط!');
        return false;
    }

    if (isNaN($("input[id$='txtVRCNoKr']").val())) {
        alert('رقم مركز التموين يجب ان يكون ارقام فقط!');
        return false;
    }

    if (($("input[id$='txtVoterIdKr']").val() == "") && ($("input[id$='txtVoterNameKr']").val() == "") && ($("input[id$='txtFatherNameKr']").val() == "") && ($("input[id$='txtGrandFatherNameKr']").val() == "") && ($("input[id$='txtFamilyNoKr']").val() == "") && ($("input[id$='txtVRCNoKr']").val() == "")) {
        alert('لايجوز ترك الحقول فارغه!');
        return false;
    }
    return true;
}

function CancelReturnKey() {
    if (window.event.keyCode == 13)
        return false;
}