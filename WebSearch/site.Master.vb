﻿Imports CodeTier

Public Class site
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myScript As String
        Try
            Dim nCount As Long
            Dim visitor As New SearchPerson
            If Session.Contents("visited") = Nothing Then
                Session.Contents("visited") = 1
                visitor.add_visitor()
            End If

            nCount = visitor.get_visitors_count()
            lblCount.Text = nCount.ToString()
            Session.Contents("visitors") = nCount.ToString
        Catch ex As Exception
            myScript = "alert('خطأ في الاتصال الرجاء المحاولة لاحقا!');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
        End Try
       
    End Sub

End Class