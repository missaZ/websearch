﻿Public Class Results
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myScript As String

        Try
            lblCount.Text = Session.Contents("visitors")

            Dim dt As New DataTable
            Dim i As Integer
            Dim k As Integer
            Dim cell As TableCell
            Dim row As TableRow
            Dim myLabel As Label
            Dim r As Integer = 0

            dt = Session("results")

            Do While r <= dt.Rows.Count - 1
                For i = 0 To dt.Columns.Count - 3 Step 3
                    row = New TableRow
                    For k = i To i + 2
                        myLabel = New Label
                        myLabel.CssClass = "labels"
                        myLabel.Text = dt.Columns(k).ColumnName

                        cell = New TableCell
                        cell.BackColor = Drawing.Color.Aqua
                        cell.CssClass = "cells"
                        cell.Controls.Add(myLabel)

                        row.Cells.Add(cell)

                        myLabel = New Label
                        myLabel.CssClass = "data"
                        myLabel.Text = dt.Rows(r).Item(k).ToString

                        cell = New TableCell
                        cell.BackColor = Drawing.Color.Bisque
                        cell.CssClass = "cells"
                        cell.Controls.Add(myLabel)

                        row.Cells.Add(cell)
                    Next
                    tblResults.Rows.Add(row)
                Next
                r += 1
                If r <= dt.Rows.Count - 1 Then
                    myLabel = New Label
                    myLabel.CssClass = "data"
                    myLabel.Text = "-"

                    cell = New TableCell
                    cell.CssClass = "cells"
                    cell.Controls.Add(myLabel)

                    row = New TableRow
                    row.Cells.Add(cell)
                    tblResults.Rows.Add(row)
                End If
                
            Loop
            lblStatus.Text = "تم العثور على " + dt.Rows.Count.ToString + " اشخاص!"
        Catch ex As Exception
            myScript = "alert('خطأ في الاتصال الرجاء المحاولة لاحقا!');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
        End Try
    End Sub

   
End Class