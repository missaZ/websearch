﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.Master" CodeBehind="Home.aspx.vb" Inherits="WebSearch.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content_ar" class="content" >
         <h2>معايير البحث</h2>
         <table dir="rtl" style="width:50%;margin: 0 auto;">
             <tr>
                 <td style="padding-top :10px">
                    <h4 id="voter_id_ar" >رقم الناخب</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtVoterIdAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H7" >اسم الناخب</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtNameAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H8" >اسم الاب</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtFatherNameAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H9" >اسم الجد</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtGrandFatherNameAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="padding-top :10px;">
                    <h4 id="H1" >رقم البطاقة التموينية</h4>
                     </td>
                 <td>
                      <asp:TextBox ID="txtFamilyNoAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
              <tr>
                 <td style="padding-top :10px">
                    <h4 id="H2" >رقم مركز التموين</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtVRCAr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="padding-top :10px">
                    <h4 id="H5" >المحافظة</h4>
                     </td>
                 <td >
                     <asp:DropDownList ID="ddlGovAr" runat="server" style="width:130px">
                         <asp:ListItem Value="1">بغداد-الرصافة</asp:ListItem>
                         <asp:ListItem Value="4">دهوك</asp:ListItem>
                         <asp:ListItem Value="5">اربيل</asp:ListItem>
                         <asp:ListItem Value="6">السليمانية</asp:ListItem>
                         <asp:ListItem Value="12">نينوى</asp:ListItem>
                         <asp:ListItem Value="14">كركوك</asp:ListItem>
                         <asp:ListItem Value="21">ديالى</asp:ListItem>
                         <asp:ListItem Value="22">الانبار</asp:ListItem>
                         <asp:ListItem Value="23">بغداد-الكرخ</asp:ListItem>
                         <asp:ListItem Value="24">بابل</asp:ListItem>
                         <asp:ListItem Value="25">كربلاء</asp:ListItem>
                         <asp:ListItem Value="26">واسط</asp:ListItem>
                         <asp:ListItem Value="27">صلاح الدين</asp:ListItem>
                         <asp:ListItem Value="28">النجف</asp:ListItem>
                         <asp:ListItem Value="31">القادسية</asp:ListItem>
                         <asp:ListItem Value="32">المثنى</asp:ListItem>
                         <asp:ListItem Value="33" >ذي قار</asp:ListItem>
                         <asp:ListItem Value="34">ميسان</asp:ListItem>
                         <asp:ListItem Value="35">البصرة</asp:ListItem>
                     </asp:DropDownList>
                  </td>
             </tr>     
             </table>
         <table  dir="rtl" style="width:50%;margin: 0 auto;">
             <tr>
                 <td>
                     <asp:Button ID="btnSearchAr" runat="server" Text="بحث" cssclass="buttons" OnClientClick="return validateInput()" />      
                 </td>
                 <td>
                      <asp:Button ID="btnNewSearchAr" runat="server" Text="بحث جديد" cssclass="buttons" />
                  </td>
             </tr>
         </table>
     </div>
    
  <div id="content_kr" class="content" >
       <h2>Search Criteria</h2>
       <table dir="rtl" style="width:40%;margin: 0 auto;">
             <tr>
                 <td style="padding-top :10px">
                         <h4 >voter id</h4>
                     </td>
                  <td >
                      <asp:TextBox ID="txtVoterIdKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
            <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H10" >Voter Name</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtVoterNameKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H11" >Father Name</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtFatherNameKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                  <td style="padding-top :10px;">
                    <h4 id="H12" >Grand Father Name</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtGrandFatherNameKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
           <tr>
                 <td style="padding-top :10px">
                    <h4 id="H3" >Family Number</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtFamilyNoKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
              <tr>
                 <td style="padding-top :10px">
                    <h4 id="H4" >VRC Number</h4>
                     </td>
                 <td >
                      <asp:TextBox ID="txtVRCNoKr" runat="server"></asp:TextBox>
                 </td>
             </tr>
            <tr>
                 <td style="padding-top :10px">
                    <h4 id="H6" >Governorate</h4>
                     </td>
                 <td >
                      <asp:DropDownList ID="ddlGovKr" runat="server" style="width:130px">
                         <asp:ListItem Value="1">به غدا</asp:ListItem>
                         <asp:ListItem Value="4">دهوَك</asp:ListItem>
                         <asp:ListItem Value="5">هةوليَر</asp:ListItem>
                         <asp:ListItem Value="6">سليَمانى</asp:ListItem>
                         <asp:ListItem Value="12">موسلَ</asp:ListItem>
                         <asp:ListItem Value="14">كه ركوك</asp:ListItem>
                         <asp:ListItem Value="21">ديالا</asp:ListItem>
                         <asp:ListItem Value="22">ئه نبار</asp:ListItem>
                         <asp:ListItem Value="23">به غدا</asp:ListItem>
                         <asp:ListItem Value="24">بابل</asp:ListItem>
                         <asp:ListItem Value="25">كه ربه لا</asp:ListItem>
                         <asp:ListItem Value="26">كوت</asp:ListItem>
                         <asp:ListItem Value="27">سه لاحه دين</asp:ListItem>
                         <asp:ListItem Value="28">نه جه ف</asp:ListItem>
                         <asp:ListItem Value="31">ديوانية</asp:ListItem>
                         <asp:ListItem Value="32">سه ماوه</asp:ListItem>
                         <asp:ListItem Value="33" >ناسريه</asp:ListItem>
                         <asp:ListItem Value="34">عه مارِه</asp:ListItem>
                         <asp:ListItem Value="35">به سرا</asp:ListItem>
                     </asp:DropDownList>
                 </td>
             </tr>
             </table>
      <table  dir="rtl" style=" width:50%;margin: 0 auto;">
          <tr>
                 <td>
                     <asp:Button ID="btnSearchKr" runat="server" Text="Search" cssclass="buttons" OnClientClick="return ValidateInputKr()" />     
                 </td>
                 <td> 
                     <asp:Button ID="btnNewSearchKr" runat="server" Text="New Search" cssclass="buttons" />      
                 </td>
             </tr>
      </table>
      </div>  
</asp:Content>
