﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Results.aspx.vb" Inherits="WebSearch.Results" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">
<head runat="server">
    <link href="CSS/Site.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script src="Scripts/jquery-1.4.1.js"></script>
    <script src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/jquery-1.8.3.min.js"></script>
    <script src="Scripts/animation.js"></script>
    <title>صفحة عرض نتيجة البحث</title>
</head>
<body >
    <form id="form1" runat="server">
    <div id="header" >
    <img  src="images/header.JPG" alt ="header" style="width:1000px"/>
	</div>
        <div id="menu">
	<ul>
        <li id="VcountLbl" style="cursor:default;">عدد الزائرين :&nbsp;&nbsp;&nbsp;<asp:Label ID="lblCount" runat="server" Text=""></asp:Label></li>
		 <li style="cursor:pointer; color:white;"><a href="Home.aspx" > الرئيسية \ Home </a></li>
        <li style="cursor:default;">&nbsp;</li>       
	</ul>
</div>
       
                               <div id="page">
                                   <div id="Results">
                                       <h2>نتيجة البحث</h2>
                                       <div align="center">
                                           
                                           <asp:Table ID="tblResults" runat="server" ></asp:Table></div>
                                       
                                   </div>
                                   <div class="Status">
                                       <asp:Label ID="lblStatus" runat="server" Text="" ></asp:Label>
                                   </div>
                                   </div>
                     

    </form>
</body>
</html>
