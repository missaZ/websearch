﻿Imports CodeTier

Public Class Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
    End Sub

    Protected Sub btnSearchAr_Click(sender As Object, e As EventArgs) Handles btnSearchAr.Click
        Dim myScript As String
        Try
            Dim p As New SearchPerson
            Dim dt As New DataTable

            If txtVoterIdAr.Text <> "" Then
                p = New SearchPerson
                p.VoterId = Integer.Parse(txtVoterIdAr.Text)
                dt = p.Search()
            ElseIf txtNameAr.Text <> "" And txtFatherNameAr.Text <> "" And txtGrandFatherNameAr.Text <> "" And txtFamilyNoAr.Text <> "" Then
                p = New SearchPerson
                p.FirstName = txtNameAr.Text
                p.SecondName = txtFatherNameAr.Text
                p.ThirdName = txtGrandFatherNameAr.Text
                p.Family_number = Integer.Parse(txtFamilyNoAr.Text.ToString, Globalization.NumberStyles.Integer)
                p.Governorate = ddlGovAr.SelectedValue
                dt = p.Search()
            ElseIf txtFamilyNoAr.Text <> "" And txtVRCAr.Text <> "" Then
                p = New SearchPerson
                p.Family_number = Integer.Parse(txtFamilyNoAr.Text.ToString, Globalization.NumberStyles.Integer)
                p.VrcId = Integer.Parse(txtVRCAr.Text.ToString, Globalization.NumberStyles.Integer)
                dt = p.Search()
            End If


            If p.Found Then
                Session("results") = dt
                Response.Redirect("Results.aspx")
            Else
                myScript = "alert('البيانات غير صحيحة او غير موجودة!');"
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
            End If

        Catch ex As Exception
            myScript = "alert('خطأ في الاتصال الرجاء المحاولة لاحقا!');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
        End Try
    End Sub

    Protected Sub btnSearchKr_Click(sender As Object, e As EventArgs) Handles btnSearchKr.Click
        Dim myScript As String
        Try
            Dim p As New SearchPerson
            Dim dt As New DataTable

            If txtVoterIdKr.Text <> "" Then
                p = New SearchPerson
                p.VoterId = Integer.Parse(txtVoterIdKr.Text)
                dt = p.Search()
            ElseIf txtVoterNameKr.Text <> "" And txtFatherNameKr.Text <> "" And txtGrandFatherNameKr.Text <> "" And txtFamilyNoKr.Text <> "" Then
                p = New SearchPerson
                p.FirstName = txtVoterNameKr.Text
                p.SecondName = txtFatherNameKr.Text
                p.ThirdName = txtGrandFatherNameKr.Text
                p.Family_number = Integer.Parse(txtFamilyNoKr.Text.ToString, Globalization.NumberStyles.Integer)
                p.Governorate = ddlGovKr.SelectedValue
                dt = p.Search()
            ElseIf txtFamilyNoKr.Text <> "" And txtVRCNoKr.Text <> "" Then
                p = New SearchPerson
                p.Family_number = Integer.Parse(txtFamilyNoKr.Text.ToString, Globalization.NumberStyles.Integer)
                p.VrcId = Integer.Parse(txtVRCNoKr.Text.ToString, Globalization.NumberStyles.Integer)
                dt = p.Search()
            End If

            If p.Found Then
                Session("results") = dt
                Response.Redirect("Results.aspx")
            Else
                myScript = "alert('البيانات غير صحيحة او غير موجودة!');"
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
            End If

        Catch ex As Exception
            myScript = "alert('خطأ في الاتصال الرجاء المحاولة لاحقا!');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
        End Try
    End Sub

    
End Class