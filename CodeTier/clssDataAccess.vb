﻿Imports System.Data.SqlClient

Public Class DataAccessLayer
    Private _dbConn As SqlConnection
    Private _connectionString As String

    Protected Sub New()
        _connectionString = ConfigurationManager.ConnectionStrings("MainConnection").ConnectionString
        _dbConn = New SqlConnection(_connectionString)
    End Sub

    Protected Sub OpenConnection()
        If Not _dbConn.State = ConnectionState.Open Then
            _dbConn.Open()
        End If
    End Sub

    Protected Sub CloseConnection()
        If Not _dbConn.State = ConnectionState.Closed Then
            _dbConn.Close()
        End If
    End Sub

    Protected Function newparameter(ByVal name As String, ByVal type As SqlDbType, ByVal value As Object) As SqlParameter
        Dim param As New SqlParameter(name, type)
        param.Value = value
        Return param
    End Function

    'Protected Sub searchPerson(ByVal spname As String, ByRef dtb As DataTable, ByVal sqlcomm As SqlCommand)
    Protected Sub SelectProcedure(ByVal spname As String, ByRef dtb As DataTable, ByVal ParamArray params() As SqlParameter)
        Try
            OpenConnection()

            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand

            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = spname
            sqlcomm.Parameters.AddRange(params)
            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtb)

            CloseConnection()

        Catch ex As Exception
            Throw New Exception(ex.Message)


        End Try
    End Sub

    Protected Sub select_query(ByVal query As String, ByRef dtb As DataTable)
        Try
            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand

            OpenConnection()
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.Text
            sqlcomm.CommandText = query
            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtb)

            CloseConnection()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Protected Function insert_query(ByVal Query As String) As Integer
        Try
            Dim sqlcomm As New SqlCommand
            Dim records As Integer

            OpenConnection()
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.Text
            sqlcomm.CommandText = Query
            records = sqlcomm.ExecuteNonQuery()
            CloseConnection()

            Return records
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return 0
        End Try
    End Function

    Protected Function insert_stdProc(ByVal spname As String, ByVal ParamArray params() As SqlParameter) As Integer
        Try
            Dim sqlcomm As New SqlCommand
            Dim records As Integer

            OpenConnection()
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = spname
            sqlcomm.Parameters.AddRange(params)
            records = sqlcomm.ExecuteNonQuery

            CloseConnection()

            Return records
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return 0
        End Try
    End Function

End Class
