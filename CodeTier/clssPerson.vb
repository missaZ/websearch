﻿Imports System.Data.SqlClient

Public Class SearchPerson
    Inherits DataAccessLayer

    Private _voterId As Integer
    Private _fam As Integer
    Private _vrc As Integer
    Private _firstName As String
    Private _secondName As String
    Private _thirdName As String
    Private _gov As Integer
    Private _found As Boolean
    Private _barCode As Long

    Public Sub New()
        MyBase.New()

        _voterId = 0
        _fam = 0
        _vrc = 0
        _firstName = ""
        _secondName = ""
        _thirdName = ""
        _gov = 0
        _found = False
    End Sub

    Public Property BarCode() As Long
        Get
            Return _barCode
        End Get
        Set(ByVal value As Long)
            _barCode = value
        End Set
    End Property

    Public Property VoterId() As Integer
        Get
            Return _voterId
        End Get
        Set(ByVal value As Integer)
            _voterId = value
        End Set
    End Property

    Public Property Governorate() As Integer
        Get
            Return _gov
        End Get
        Set(ByVal value As Integer)
            _gov = value
        End Set
    End Property

    Public Property Family_number() As Integer
        Get
            Return _fam
        End Get
        Set(ByVal value As Integer)
            _fam = value
        End Set
    End Property

   
    Public Property VrcId() As Integer
        Get
            Return _vrc
        End Get
        Set(ByVal value As Integer)
            _vrc = value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property

    Public Property SecondName() As String
        Get
            Return _secondName
        End Get
        Set(ByVal value As String)
            _secondName = value
        End Set
    End Property

    Public Property ThirdName() As String
        Get
            Return _thirdName
        End Get
        Set(ByVal value As String)
            _thirdName = value
        End Set
    End Property

    Public ReadOnly Property Found() As Boolean
        Get
            Return _found
        End Get

    End Property

    Public Function Search() As DataTable
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable

            ReDim parmarray(-1)

            Dim sqlcomm As New SqlCommand
            If _voterId <> 0 Then
                ReDim parmarray(0)
                parmarray.SetValue(newparameter("@PER_ID", SqlDbType.BigInt, _voterId), 0)
            ElseIf _fam <> 0 And _vrc <> 0 Then
                ReDim parmarray(1)
                parmarray.SetValue(newparameter("@VRCID", SqlDbType.BigInt, _vrc), 0)
                parmarray.SetValue(newparameter("@FamNo", SqlDbType.BigInt, _fam), 1)
            ElseIf _fam <> 0 And _firstName <> "" And _secondName <> "" And ThirdName <> "" And _gov <> 0 Then
                ReDim parmarray(4)
                parmarray.SetValue(newparameter("@FamNo", SqlDbType.BigInt, _fam), 0)
                parmarray.SetValue(newparameter("@PER_FIRST", SqlDbType.NVarChar, _firstName), 1)
                parmarray.SetValue(newparameter("@PER_FATHER", SqlDbType.NVarChar, _secondName), 2)
                parmarray.SetValue(newparameter("@PER_GRAND", SqlDbType.NVarChar, _thirdName), 3)
                parmarray.SetValue(newparameter("@GOV_MOT_ID", SqlDbType.Int, _gov), 4)
            Else
                _found = False
                Return Nothing
            End If

            SelectProcedure("SP_PersonSearch", dtResult, parmarray)

            If dtResult.Rows.Count = 0 Then
                _found = False
                Return Nothing
            Else
                _found = True
                Return dtResult
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return Nothing

    End Function

    Public Function get_visitors_count() As Long
        Try
            Dim dtVisitors As New DataTable

            select_query("SELECT COUNT(id) FROM counters", dtVisitors)

            If dtVisitors Is Nothing Then
                Return 0
            End If
            Return dtVisitors.Rows(0)(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return 0
        End Try
    End Function

    Public Sub add_visitor()
        Try

            Dim parmarray() As SqlParameter
            Dim sqlcomm As New SqlCommand
           
            parmarray = {newparameter("@VisitDate", SqlDbType.DateTime, Date.Now)}
            insert_stdProc("SP_AddVisitor", parmarray)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function searchRejected() As DataTable
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable

            ReDim parmarray(-1)

            Dim sqlcomm As New SqlCommand
            If _barCode <> 0 Then
                ReDim parmarray(0)
                parmarray.SetValue(newparameter("@BARCODE", SqlDbType.BigInt, _barCode), 0)
            ElseIf _firstName <> "" And _secondName <> "" And _thirdName <> "" Then
                ReDim parmarray(2)
                parmarray.SetValue(newparameter("@PER_FIRST", SqlDbType.NVarChar, _firstName), 0)
                parmarray.SetValue(newparameter("@PER_FATHER", SqlDbType.NVarChar, _secondName), 1)
                parmarray.SetValue(newparameter("@PER_GRAND", SqlDbType.NVarChar, _thirdName), 2)
            Else
                _found = False
                Return Nothing
            End If

            SelectProcedure("SP_SearchRejected", dtResult, parmarray)

            If dtResult.Rows.Count = 0 Then
                _found = False
                Return Nothing
            Else
                _found = True
                Return dtResult
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return Nothing

    End Function

    
End Class
